<?php 
    namespace server\core;

    use server\core\Handler_Authentications;

    class Handler_HTTP extends Handler_Authentications {

        protected $headers;
        protected $method;
        protected $url;
        protected $params;
        protected $user;

        public function __construct() {
            header("Access-Control-Allow-Origin: *");
            header("Content-Type: application/json; charset=UTF-8");
            header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
            header("Access-Control-Max-Age: 600");
            header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

            $this->method   = $_SERVER['REQUEST_METHOD'];
            $this->url      = $_SERVER['REDIRECT_URL'];
            $this->params   = $_SERVER['REQUEST_URI'];

            session_start();
            
            $this->user = parent::__construct(); // instancea la clase user para validar si el usuario esta loggeado para luego insertar la data del mismo a las respuesta a las solicitudes
        }
        
        private function parseRequest($type = false) { // funcion privada parseRequest procesa los parametros de la solicitud dependiendo del bandera que se le pase a esta funcion
            if(!$type){ // validar si los parametros de la solicitud son acceptador por apache ($_GET, $_POST), de lo contrario los parametros se procesesaran con funcion file_get_contents de php 
                $response = json_decode(file_get_contents('php://input'), true);
                return array(
                    'user' => $this->user,
                    'body' => $response
                );
            }else  
                return array(
                    'user' => $this->user,
                    'query' => $_GET
                );
        }

        private function parseResponse($response) { // funcion privada parseResponse procesa la informacion de respuesta y errores.
            if($response){
                echo json_encode($response);
                return;
            }else {
                echo json_encode(array(
                    'err' => 404,
                    'm'    => 'Not Found'
                ));
                return;
            }
        }

        public function get($uri, $callback) { // funcion get para manejar las solicitudes por el metodo get, al cual se debe pasar por paramtros de la funcion la url a la cual se debe ejecutar dicha funcion que vuelve un callback con los paramtro de la solicitud y funcion para manejar la respuesta.
            if($this->method === 'GET'){ // verificacion de el metodo de la solicitud.
                if('/'.$uri === $this->url){ // verificacion de la url por parametros de la funcion y de la solicitud.
                    $request = $this->parseRequest(true);
                    $callback($request, function($response){ 
                        $this->parseResponse($response); 
                    });
                }else if('/'.preg_replace('/[:][a-zA-Z]+/','',$uri) === preg_replace('/[0-9]/','',$this->url)) { // verificacion de la url por parametros de la funcion y de la solicitud si el ID viene en ella.
                    
                    $values   = explode('/',preg_replace('/[\/]+/','',preg_replace('/[\/][a-zA-Z]+/','',$this->url),1));
                    $entities = explode('/',preg_replace('/[\/]+/','',preg_replace('/[\/][:]+/','/',preg_replace('/[\/][a-zA-z]+/','','/'.$uri)),1));
                    
                    $params = array_combine($entities, $values);

                    $request = array_merge(
                        array(
                            'params' => $params
                        ),
                        $this->parseRequest(true) // ejectar la funcion parseRequest que va a retornar los parametro de la solicitud.
                    ); // construccion de la solicitud.
                    $callback($request, function($response){ 
                        $this->parseResponse($response); 
                    }); // callback.
                }else return false;
            }else return false;
        }

        public function post($uri, $callback) { // funcion get para manejar las solicitudes por el metodo post, al cual se debe pasar por paramtros de la funcion la url a la cual se debe ejecutar dicha funcion que vuelve un callback con los paramtro de la solicitud y funcion para manejar la respuesta.
            if($this->method === 'POST'){
                if('/'.$uri === $this->url){
                    $request = $this->parseRequest(); 
                    return $callback($request, function($response){ 
                        return $this->parseResponse($response); 
                    });
                }else if('/'.preg_replace('/[:][a-zA-Z]+/','',$uri) === preg_replace('/[0-9]/','',$this->url)) {
                    $values   = explode('/',preg_replace('/[\/]+/','',preg_replace('/[\/][a-zA-Z]+/','',$this->url),1));
                    $entities = explode('/',preg_replace('/[\/]+/','',preg_replace('/[\/][:]+/','/',preg_replace('/[\/][a-zA-z]+/','','/'.$uri)),1));

                    $params = array_combine($entities, $values);
                    
                    $request = array_merge(
                        array(
                            'query' => $_GET,
                            'params' => $params
                        ),
                        $this->parseRequest()
                    );
                    return $callback($request, function($response){ 
                        return $this->parseResponse($response); 
                    });
                }else return false;
            }else return false;
        }

        public function put($uri, $callback) { // funcion get para manejar las solicitudes por el metodo put, al cual se debe pasar por paramtros de la funcion la url a la cual se debe ejecutar dicha funcion que vuelve un callback con los paramtro de la solicitud y funcion para manejar la respuesta.
            if($this->method === 'PUT'){
                if('/'.$uri === $this->url){
                    $request = $this->parseRequest(); 
                    $callback($request, function($response){ 
                        $this->parseResponse($response); 
                    });
                }else if('/'.preg_replace('/[:][a-zA-Z]+/','',$uri) === preg_replace('/[0-9]/','',$this->url)) {
                    $values   = explode('/',preg_replace('/[\/]+/','',preg_replace('/[\/][a-zA-Z]+/','',$this->url),1));
                    $entities = explode('/',preg_replace('/[\/]+/','',preg_replace('/[\/][:]+/','/',preg_replace('/[\/][a-zA-z]+/','','/'.$uri)),1));

                    $params = array_combine($entities, $values);
                    
                    $request = array_merge(
                        array(
                            'query' => $_GET,
                            'params' => $params
                        ),
                        $this->parseRequest()
                    );
                    $callback($request, function($response){ 
                        $this->parseResponse($response); 
                    });
                }else return false;
            }else return false;
        }

        public function delete($uri, $callback) { // funcion delete para manejar las solicitudes por el metodo get, al cual se debe pasar por paramtros de la funcion la url a la cual se debe ejecutar dicha funcion que vuelve un callback con los paramtro de la solicitud y funcion para manejar la respuesta.
            if($this->method === 'DELETE'){
                if('/'.$uri === $this->url){
                    $request = $this->parseRequest();
                    $callback($request, function($response){ 
                        $this->parseResponse($response); 
                    });
                }else if('/'.preg_replace('/[:][a-zA-Z]+/','',$uri) === preg_replace('/[0-9]/','',$this->url)) {
                    $values   = explode('/',preg_replace('/[\/]+/','',preg_replace('/[\/][a-zA-Z]+/','',$this->url),1));
                    $entities = explode('/',preg_replace('/[\/]+/','',preg_replace('/[\/][:]+/','/',preg_replace('/[\/][a-zA-z]+/','','/'.$uri)),1));

                    $params = array_combine($entities, $values);
                    
                    $request = array_merge(
                        array(
                            'query' => $_GET,
                            'params' => $params
                        ),
                        $this->parseRequest()
                    );
                    $callback($request, function($response){ 
                        $this->parseResponse($response); 
                    });
                }else return false;
            }else return false;
        }

        public function use($callback) { 
            if(array_key_exists('HTTP_REFERER', $_SERVER)){
                $request = $this->parseRequest(true);
                $callback($request, function($response){
                    $this->parseResponse($response);
                });
            }
        }
        
    }    
