<?php 

    namespace server\core;

    use server\core\Handler_Database;
    use server\core\APIException;

    class Handler_SQLBuilder extends Handler_Database {

        protected $hdb;
        protected $table;
        protected $sql;
        protected $values;
        protected $type;
        
        public function __construct($table) {
            $this->hdb = parent::getInstance();

            if($table)
                $this->table = $table;
            else 
                throw new APIException('Error:','Debes enviar la tabla para hacer la operacion correspondiente');
        }

        public function getSQL() {
			return $this->sql;
        }
        
        public function getValues() {
			return $this->values;
		}

        public function find($query = false) {
            $this->type = 0;
            $this->sql = " SELECT * FROM {$this->table} ";
            if(is_array($query)) {
                $this->sql .= $this->where($query);
            }else if($query) {
                $this->sql .= " WHERE {$query} ";
            }
        }
        
        public function findOne($query) {
            $this->type = 1;
            $this->sql = " SELECT * FROM {$this->table} ";
            if(is_array($query)) {
                $this->sql .= $this->where($query);
            }else if($query) {
                $this->sql .= " WHERE {$query} ";
            }
        }
            
        public function insert($fields) {
            $this->type = 2;
            $entities = array();
            $values = array();
            $valuep = array();
            if($fields && is_array($fields)){
                foreach($fields as $key => $array) {
                    if(is_array($array)) 
                        $values[] = implode(",", $array);
                    else
                        $values[] = $array;
                    $entities[] = $key;
                    $valuep[] = ' ? ';                    
                }
                $field = '`'.implode("`,`", $entities).'`';
                $value = implode(",", $valuep);
                $this->sql = " INSERT INTO {$this->table} ($field) VALUES ($value) ";
                $this->values = array_values($values);
            }else throw new APIException('Error:','Debes enviar parametros para insertar');
        }

        public function update($fields ,$query) {
            $this->type = 3;
            $entities = array();
            $values = array();
            $valuep = array();
            if($fields && is_array($fields)) {
                foreach($fields as $key => $array) {
                    if(is_array($array)) 
                        $values[] = implode(",", $array);
                    else
                        $values[] = $array;
                    $entities[] = $key.'=  ? ';                    
                }
                $field = implode(",", $entities);
                $this->sql = " UPDATE {$this->table} SET $field ";

                if(is_array($query)) {
                    $this->sql .= $this->where($query);
                }else if($query) {
                    $this->sql .= " WHERE {$query} ";
                }
                $this->values = array_values($values);
                
            }else throw new APIException('Error:','Debes enviar parametros para actualizar');
        }

        public function remove($query) {
            $this->type = 4;
            $this->sql = " DELETE FROM {$this->table} ";
            if(is_array($query)) {
                $this->sql .= $this->where($query);
            }else if($query) {
                $this->sql .= " WHERE {$query} ";
            }
        }

        public function where($query) {
            if(is_array($query)) {
                $return = ' WHERE ';
                $i = 0;
                $e = 0;
                foreach($query as $operator => $array) {
                    $e++;
                    foreach($array as $table => $value) {
                        $i++;
                        $return .= " {$table} {$operator} {$this->hdb->quote($value)} ";
                        
                        if(count($array)>1 && $i<count($array)){
                            $return .= ' AND ';
                        } 
                    }
                    if(count($query)>1 && $e<count($query)){
                        $return .= ' AND ';
                    } 
                }
                return $return;
            }else if(is_string($query)) {
                return $return .= $query;
            }
            else throw new APIException('Error:','el parametro $query no es un array');
        } 

        public function count($rows) {
            if(is_array($rows))
                return count($rows);
            else
                return false;
        }

        public function sort($query) {
            if(is_array($query)){
               $return = ' ORDER BY ';
                $i = 0;
                foreach($query as $operator => $array) {
                    foreach($array as $table => $value) {
                        $i++;
                        $return .= " {$table} {$operator} {$this->hdb->quote($value)} ";
                        if(count($array)>1 && count($array)<$i) $return .= ' AND ';
                    }
                }
                return $return;
            }else if(is_string($query)) {
                return $return .= $query;
            } 
            else throw new APIException('Error:','el parametro $query no es un array');  
        }

        public function limit(int $start,int $limit) {
            $return = "";
            $start = $start ? $start: 0;
            $limit = $limit ? $limit: 0;
            if(intval($start))
                $return .= " OFFSET {$start}";
            if(intval($limit))
                $return .= " LIMIT {$limit}";
            
            return $return ? $return : false;
        }

        public function between($start, $end) {
            if($start && $end)
                $return = " BETWEEN {$start} AND {$end}"; 
            return $return ? $return : false;
        }

        public function execute() {
            switch($this->type) {
                case 0:
                    return $this->hdb->findQuery($this);
                    break;
                case 1:
                    return $this->hdb->findOneQuery($this);
                    break;
                case 2:
                    return $this->hdb->insertQuery($this);
                    break;
                case 3:
                    return $this->hdb->updateQuery($this);
                    break;
                case 4: 
                    return $this->hdb->deleteQuery($this);
                    break;
            }
        }
        
    }