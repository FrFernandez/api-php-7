<?php 

	define("DB", [
			"DB_SERVER"	   => "mysql",
			"DB_HOST_NAME" => "localhost",
			"DB_DATABASE"  => "test_php",
			"DB_USER_NAME" => "root",
			"DB_PASSWORD"  => "123456789",
			"DB_CHARSET"   => "utf8",
			"DB_OPTIONS"   => array(
				PDO::ATTR_PERSISTENT => true
			)
	]);

	define("MAIL_CREDENTIALS",[
			"MAIL"      => false,
			"PASSWORD"  => false,
			"TYPE"      => false 
	]);

	// $http->post('users/id/:id',function($req, $res) { 
	// 	$collection = new Handler_SQLBuilder('table_users');
	// 	$collection->insert($req['body']);
	// 	$response = $collection->execute();
	// 	$res($response);
	// });

	// $http->put('users',function($req, $res) { 
	// 	$collection = new Handler_SQLBuilder('table_users');
	// 	$response = $collection->update($req['body']);
	// 	$response = $collection->execute();
	// 	$res($response);
	// });

	// Structure WHERE and ORDER BY
	// array(
	//     '=' => array(
	//         'table' => 'value'
	//     )
	// )
